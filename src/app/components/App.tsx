import React from 'react';
import { AppProps } from 'constants/types';

import Header from './Header';

const App = ({ children }: AppProps) => {
  return (
    <div>
      <Header />
      {children}
    </div>
  );
};

export default App;
