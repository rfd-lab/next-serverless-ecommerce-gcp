const submitErrorAttr = {
  "submit-error": "",
};

const submitSuccessAttr = {
  "submit-success": "",
};

const ampCustomAttr = {
  "amp-custom": "",
};

export const AmpCustom = (
  <>
    <style jsx {...ampCustomAttr}>
      {`
        form.amp-form-submit-success > input {
          display: none;
        }
      `}
    </style>
  </>
);

export default () => (
  <div className="flex flex-column">
    <h1 className="h3 mb2">Sign Up</h1>
    <hr className="xs-hide sm-hide mt3 mb3" />

    <form
      id="signUpForm"
      method="post"
      action-xhr="/api/auth/signup"
      encType="application/x-www-form-urlencoded"
      target="_top"
      className="hide-inputs"
    >
      <div className="ampstart-input inline-block relative m0 p0 mb3">
        <input
          type="text"
          value=""
          name="displayName"
          id="displayName"
          className="block border-none p0 m0"
          placeholder="Full name"
          required
          autoComplete="on"
        />
        <label
          htmlFor="displayName"
          className="absolute top-0 right-0 bottom-0 left-0"
          aria-hidden="true"
        >
          Full name
        </label>
      </div>
      <div className="ampstart-input inline-block relative m0 p0 mb3">
        <input
          type="email"
          value=""
          name="email"
          id="email"
          className="block border-none p0 m0"
          placeholder="Email address"
          required
          autoComplete="on"
        />
        <label
          htmlFor="email"
          className="absolute top-0 right-0 bottom-0 left-0"
          aria-hidden="true"
        >
          Email
        </label>
      </div>
      <div className="ampstart-input inline-block relative m0 p0 mb3">
        <input
          type="password"
          value=""
          name="password"
          id="password"
          className="block border-none p0 m0"
          placeholder="Password"
          required
          autoComplete="on"
        />
        <label
          htmlFor="password"
          className="absolute top-0 right-0 bottom-0 left-0"
          aria-hidden="true"
        >
          Password
        </label>
      </div>
      <div className="ampstart-input inline-block relative m0 p0 mb3">
        <input
          type="tel"
          value=""
          name="phoneNumber"
          id="phoneNumber"
          className="block border-none p0 m0"
          placeholder="Phone"
          required
          autoComplete="on"
        />
        <label
          htmlFor="phoneNumber"
          className="absolute top-0 right-0 bottom-0 left-0"
          aria-hidden="true"
        >
          Phone
        </label>
      </div>
      <div className="ampstart-input inline-block relative m0 p0 mb3">
        <input
          type="url"
          value=""
          name="photoURL"
          id="photoURL"
          className="block border-none p0 m0"
          placeholder="Photo URL"
          required
          autoComplete="on"
        />
        <label
          htmlFor="photoURL"
          className="absolute top-0 right-0 bottom-0 left-0"
          aria-hidden="true"
        >
          Photo URL
        </label>
      </div>
      <button className="ampstart-btn ampstart-btn-secondary caps">send</button>
      <button
        type="submit"
        className="ampstart-btn ampstart-btn-secondary caps ml2"
        on="tap:signUpForm.clear"
      >
        reset
      </button>
      <div {...submitSuccessAttr}>Success</div>
      <div {...submitErrorAttr}>
        <template
          type="amp-mustache"
          dangerouslySetInnerHTML={{
            __html: `Error! {{error}}`,
          }}
        />
      </div>
    </form>
  </div>
);
