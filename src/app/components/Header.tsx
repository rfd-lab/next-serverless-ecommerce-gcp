import React from 'react';
import dynamic from 'next/dynamic';

const DynamiEcommerceNavbar = dynamic(() =>
  import('../components/e-commerce/Navbar')
);

export default () => <DynamiEcommerceNavbar />;
