const initialProperty = {
  'amp-boilerplate': ''
};

export const NoScriptStyle = (
  <noscript>
    <style jsx {...initialProperty}>
      {`
        body {
          -webkit-animation: none;
          -moz-animation: none;
          -ms-animation: none;
          animation: none;
        }
      `}
    </style>
  </noscript>
);

export const AmpBoilerplate = (
  <>
    <style jsx {...initialProperty}>
      {`
        body {
          -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
          -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
          -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
          animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        }
      `}
    </style>
    {NoScriptStyle}
  </>
);
