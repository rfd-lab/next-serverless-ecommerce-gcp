import React from 'react';
import Sidebar from './Sidebar';

export default function Navbar() {
  return (
    <>
      <header className="ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4 pt2 md-pt0">
        <div
          role="button"
          aria-label="open sidebar"
          on="tap:header-sidebar.toggle"
          tabIndex={0}
          className="ampstart-navbar-trigger pr2 absolute top-0 pr0 mr2 mt2"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            className="block"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path
              fill="currentColor"
              d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"
            />
          </svg>
        </div>
        <a
          href="/"
          className="text-decoration-none inline-block mx-auto ampstart-headerbar-home-link mb1 md-mb0 "
        >
          <amp-img
            src="/img/e-commerce/logo.png"
            srcset="/img/e-commerce/logo.png"
            width="96"
            height="27"
            layout="fixed"
            className="my0 mx-auto "
            alt=""
          />
        </a>

        <div className="ampstart-headerbar-fixed center m0 p0 flex nowrap absolute top-0 right-0 pt2 pr3 mr3">
          <a
            href="signup"
            className="text-decoration-none mr2 ampstart-headerbar-fixed-link"
          >
            <div className="ampstart-headerbar-icon-wrapper relative">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <g>
                  <path d="M12.075,10.812c1.358-0.853,2.242-2.507,2.242-4.037c0-2.181-1.795-4.618-4.198-4.618S5.921,4.594,5.921,6.775c0,1.53,0.884,3.185,2.242,4.037c-3.222,0.865-5.6,3.807-5.6,7.298c0,0.23,0.189,0.42,0.42,0.42h14.273c0.23,0,0.42-0.189,0.42-0.42C17.676,14.619,15.297,11.677,12.075,10.812 M6.761,6.775c0-2.162,1.773-3.778,3.358-3.778s3.359,1.616,3.359,3.778c0,2.162-1.774,3.778-3.359,3.778S6.761,8.937,6.761,6.775 M3.415,17.69c0.218-3.51,3.142-6.297,6.704-6.297c3.562,0,6.486,2.787,6.705,6.297H3.415z" />
                </g>
              </svg>
            </div>
          </a>
        </div>

        <div className="ampstart-headerbar-fixed center m0 p0 flex nowrap absolute top-0 right-0 pt2 mr2">
          <a
            href="cart"
            className="text-decoration-none mr2 ampstart-headerbar-fixed-link"
          >
            <div className="ampstart-headerbar-icon-wrapper relative">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 35">
                <g>
                  <path
                    d="M.8.47598c-.4 0-.8.3-.8.8s.4.9.8.9h4.4l4.4 21.6c0 .5.4 1 .9.9h21c.4 0 .9-.4.9-.9s-.4-.9-.9-.9H11.2l-.7-3.5h22.7c.4 0 .7-.3.8-.7l2.9-13c.1-.5-.3-1.1-.8-1.1H7.5l-.8-3.5c-.1-.3-.4-.6-.8-.6H.8zm7 6h27.3l-2.6 11.3H10.1l-2.3-11.3zm6.9 19.9c-2.1 0-3.8 1.8-3.8 3.9s1.7 3.9 3.8 3.9c2.1 0 3.8-1.8 3.8-3.9 0-2.1-1.7-3.9-3.8-3.9zm12.6 0c-2.1 0-3.8 1.8-3.8 3.9s1.7 3.9 3.8 3.9c2.1 0 3.8-1.8 3.8-3.9 0-2.1-1.7-3.9-3.8-3.9zm-12.6 1.7c1.2 0 2.1 1 2.1 2.2 0 1.2-.9 2.2-2.1 2.2-1.2 0-2.1-1-2.1-2.2 0-1.2.9-2.2 2.1-2.2zm12.6 0c1.2 0 2.1 1 2.1 2.2 0 1.2-.9 2.2-2.1 2.2-1.2 0-2.1-1-2.1-2.2 0-1.2 1-2.2 2.1-2.2z"
                    fill="#222"
                  />
                </g>
              </svg>
            </div>
          </a>
        </div>
      </header>

      <Sidebar />
    </>
  );
}
