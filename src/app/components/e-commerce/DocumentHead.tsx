import { Head } from 'next/document';
import React from 'react';
import { AmpBoilerplate } from '../amp/AmpBoilerplate';

export default function DocumentHead() {
  return (
    <Head>
      <meta charSet="utf-8" />
      <meta name="description" content="This is the AMP Boilerplate." />
      <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js" />
      <link
        as="script"
        href="https://cdn.ampproject.org/v0/amp-experiment-0.1.js"
      />
      <link
        as="script"
        href="https://cdn.ampproject.org/v0/amp-dynamic-css-classes-0.1.js"
      />
      <script async src="https://cdn.ampproject.org/v0.js" />
      <script
        async
        custom-element="amp-analytics"
        src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"
      />
      <script
        custom-element="amp-sidebar"
        src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"
        async
      />
      <script
        async
        custom-element="amp-font"
        src="https://cdn.ampproject.org/v0/amp-font-0.1.js"
      />
      <script
        async
        custom-element="amp-carousel"
        src="https://cdn.ampproject.org/v0/amp-carousel-0.2.js"
      ></script>
      <link rel="manifest" href="/manifest.json" />
      <meta name="description" content="AMP e-commerce template" />
      <meta name="keywords" content="amp, startup" />
      {AmpBoilerplate}
    </Head>
  );
}
