import { NextApiResponse } from 'next';

export type AppProps = {
  props?: any;
  children?: any;
};

export interface UserInfo {
  email: string | undefined;
  emailVerified: boolean | undefined;
  phoneNumber: string | undefined;
  password: string | undefined;
  displayName: string | undefined;
  photoURL: string | undefined;
  disabled: boolean | undefined;
}

type cookieFn = (name: any, value: any, options: any) => any;

export interface ApiResponse extends NextApiResponse {
  statusCode: number;
  error?: object;
}

export interface SignUpResponse extends ApiResponse {
  user?: object;
  cookie?: cookieFn;
}

export interface SignInResponse extends ApiResponse {
  credential?: firebase.auth.UserCredential;
  cookie: cookieFn;
}

export interface Product {
  name: string;
  description: string;
  price: number;
  image: string;
  category: string;
}
