const withPlugins = require('next-compose-plugins');
const withPWA = require('next-pwa');

const dev = process.env.NODE_ENV !== 'production';

const pwa = withPWA({
  pwa: {
    dest: 'public',
    disable: true,
    maximumFileSizeToCacheInBytes: 4 * 1024 * 1024
  }
});

const nextConfig = {
  devIndicators: {
    autoPrerender: !!dev
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.test.js$/,
      loader: 'ignore-loader'
    });
    return config;
  }
};

module.exports = dev ? nextConfig : withPlugins([pwa], nextConfig);
