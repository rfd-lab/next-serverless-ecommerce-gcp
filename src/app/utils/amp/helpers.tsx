import React from 'react';

export function dataScript(data: object): JSX.Element {
  return (
    <script
      type="application/json"
      dangerouslySetInnerHTML={{
        __html: JSON.stringify(data)
      }}
    />
  );
}

export default dataScript;
