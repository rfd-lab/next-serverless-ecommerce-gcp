import { auth } from './firebaseAuthService';
import {
  UserInfo,
  SignUpResponse,
  SignInResponse
} from '../../constants/types';
import admin from './firebaseAdminService';

const code = 'authService/create-user-account';

export const createUserAccount = async (
  userInfo: UserInfo
): Promise<Partial<SignUpResponse>> => {
  if (!userInfo || Object.entries(userInfo).length === 0) {
    return {
      statusCode: 500,
      error: { code, message: 'user info was not provided' }
    };
  }

  try {
    const user = await admin.auth().createUser({ ...userInfo });

    return { statusCode: 200, user };
  } catch (err) {
    return { statusCode: 500, error: err };
  }
};

export const loginUser = async (
  email: string,
  password: string
): Promise<Partial<SignInResponse>> => {
  if (!email) {
    return {
      statusCode: 500,
      error: { code, message: 'user email was not provided' }
    };
  }

  if (!password) {
    return {
      statusCode: 500,
      error: { code, message: 'user password was not provided' }
    };
  }

  try {
    await auth().setPersistence(auth.Auth.Persistence.NONE);
    const credential = await auth().signInWithEmailAndPassword(email, password);

    return { statusCode: 200, credential };
  } catch (err) {
    return { statusCode: 500, error: err };
  }
};

export const getToken = async () => {
  const token = await auth().currentUser?.getIdToken();

  return { token: `${token}` };
};
