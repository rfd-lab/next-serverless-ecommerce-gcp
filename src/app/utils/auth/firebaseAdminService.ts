import * as admin from 'firebase-admin/lib';

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: process.env.databaseURL
});

export default admin;
