import { NextPageContext, NextComponentType } from 'next';
import { Store } from '../lib/redux/store';
import EcommerceIndexPage from './e-commerce/index';
import React from 'react';

interface IndexPageContext extends NextPageContext {
  store: Store;
}

const IndexPage: NextComponentType<IndexPageContext> = () => (
  <EcommerceIndexPage />
);

export default IndexPage;
