import Document, { Html, Main, NextScript } from 'next/document';
import dynamic from 'next/dynamic';
import { ServerStyleSheet } from 'styled-components';
import { pageCSS } from '../styles/e-commerce/documentStyle';

const DynamicHead = dynamic(() => import('../components/e-commerce/DocumentHead'));

const DynamicFooter = dynamic(() => import('../components/e-commerce/Footer'));

export const config = { amp: true };

class EnhancedDocument extends Document {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) =>
            sheet.collectStyles(<App {...props} />)
        });

      const initialProps = await Document.getInitialProps(ctx);

      return {
        ...initialProps,
        styles: (
          <>
            <style
              dangerouslySetInnerHTML={{
                __html: pageCSS
              }}
            />
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      };
    } finally {
      sheet.seal();
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render() {
    return (
      <Html lang="en-UK">
        <DynamicHead />
        <body>
          <amp-font
            layout="nodisplay"
            font-family="Rubik"
            timeout="3000"
            on-error-add-class="no-rubik"
          />

          <amp-install-serviceworker
            src="/sw.js"
            data-iframe-src="https://amp.dev/serviceworker"
            layout="nodisplay"
          />

          <amp-analytics
            config="https://www.googletagmanager.com/amp.json?id=GTM-MW3DDJC"
            data-credentials="include"
          />

          <Main />

          <NextScript />

          <DynamicFooter />
        </body>
      </Html>
    );
  }
}

export default EnhancedDocument;
