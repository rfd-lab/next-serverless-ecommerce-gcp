import React from 'react';
import App from '../../components/App';
import styled from 'styled-components';

const Title = styled.h3`
  font-size: 50px;
  color: ${({ theme }) => theme.colors.primary};
`;

export default function About() {
  return (
    <App>
      <Title>About Page</Title>
    </App>
  );
}

About.displayName = 'About';
