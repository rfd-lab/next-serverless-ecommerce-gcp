import Head from 'next/head';
import App from '../../components/App';

export default () => (
  <>
    <Head>
      <script
        custom-element="amp-carousel"
        src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"
        async
      />
    </Head>
    <App>
      <main
        id="content"
        role="main"
        className="main items-start commerce-cart pb3 md-pb7 md-mb3"
      >
        <div className="flex flex-wrap">
          <section className="col-12 md-col-8 pt2 pb3 md-pt4 md-pb7">
            <h1 className="h3 mb2 mx2 md-mx4 ">Your Basket</h1>
            <div className="commerce-cart-item flex flex-wrap items-center mx2 mb2 md-mx4 md-mt3 md-pb3">
              <div className="col-3 sm-col-2 md-col-2 lg-col-2">
                <amp-img
                  className="commerce-cart-item-image"
                  src="/img/e-commerce/product/product-8.jpg"
                  width="1"
                  height="1"
                  layout="responsive"
                  alt="Caliper Brakes"
                  noloading=""
                >
                  <div placeholder="" className="commerce-loader" />{' '}
                </amp-img>
              </div>
              <div className="commerce-cart-item-desc px1 col-6 sm-col-7 md-col-7 lg-col-7">
                <div className="h6 mb1">Caliper Brakes</div>
                <div>Fits most wheel sizes and designed to last long.</div>
              </div>
              <div className="commerce-cart-item-price col-3 h6 flex flex-wrap justify-around items-start">
                <span>£349</span>
                <span>1</span>
                <div
                  role="button"
                  className="inline-block commerce-cart-icon"
                  tabIndex={0}
                >
                  ✕
                </div>
              </div>
            </div>
            <div className="commerce-cart-item flex flex-wrap items-center mx2 mb2 md-mx4 md-mt3 md-pb3">
              <div className="col-3 sm-col-2 md-col-2 lg-col-2">
                <amp-img
                  className="commerce-cart-item-image"
                  src="/img/e-commerce/product/product-1.jpg"
                  width="1"
                  height="1"
                  layout="responsive"
                  alt="Sprocket Set"
                  noloading=""
                >
                  <div placeholder="" className="commerce-loader" />{' '}
                </amp-img>
              </div>
              <div className="commerce-cart-item-desc px1 col-6 sm-col-7 md-col-7 lg-col-7">
                <div className="h6 mb1">Sprocket Set</div>
                <div>Steel, designed for long lasting stability.</div>
              </div>
              <div className="commerce-cart-item-price col-3 h6 flex flex-wrap justify-around items-start">
                <span>£470</span>
                <span>1</span>
                <div
                  role="button"
                  className="inline-block commerce-cart-icon"
                  tabIndex={0}
                >
                  ✕
                </div>
              </div>
            </div>
          </section>
          <section className="px2 pt3 pb4 md-px3 md-pt4 col-12 md-col-4 commerce-cart-summary commerce-side-panel">
            <h2 className="h5 md-mt3">Order Summary</h2>
            <dl className="flex flex-wrap">
              <dt className="col-10">Subtotal</dt>
              <dd className="m0 col-2 pb1 right-align">&#163;698</dd>
              <dt className="col-10 pb1">Shipping</dt>
              <dd className="m0 col-2 pb1 right-align">&#163;30</dd>
              <dt className="commerce-cart-total col-10 pt2 pb2 bold mt3 mb2">
                Total
              </dt>
              <dd className="commerce-cart-total m0 col-2 pt2 pb2 right-align bold mt3 mb2">
                &#163;753
              </dd>
            </dl>
            <p className="pb3">
              Donec id elit non mi porta gravida at eget metus. Donec
              ullamcorper nulla non metus auctor fringilla.
            </p>
            <div className="commerce-cart-actions center">
              <a
                href="checkout"
                className="ampstart-btn ampstart-btn-secondary caps"
              >
                checkout
              </a>
            </div>
          </section>
        </div>
        <section className="commerce-related-products col-12 px2 md-mt5 md-px4 xs-hide sm-hide">
          <div className="col-12 mt3 md-mt4">
            <h2 className="h5 md-h4">You may also like</h2>
            <amp-carousel
              height="170"
              layout="fixed-height"
              type="carousel"
              className="px4"
            >
              <ul className="list-reset">
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-1.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Sprocket Set"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Sprocket Set
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-2.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Fixie Blue"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Fixie Blue
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-3.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Chain set"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Chain set
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-4.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Leather Saddle"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Leather Saddle
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-5.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="16-Speed"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      16-Speed
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-6.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Red Cruiser"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Red Cruiser
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-7.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Horn Handles"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Horn Handles
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-8.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Caliper Brakes"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Caliper Brakes
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-9.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Road Bike"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Road Bike
                    </h2>
                  </a>
                </li>
                <li className="commerce-related-product inline-block mr2">
                  <a href="product_details" className="text-decoration-none">
                    <amp-img
                      className="mb2"
                      src="/img/e-commerce/product/product-10.jpg"
                      width="1"
                      height="1"
                      layout="responsive"
                      alt="Wheel Set"
                      noloading=""
                    >
                      <div placeholder="" className="commerce-loader" />{' '}
                    </amp-img>
                    <h2 className="commerce-related-product-name h6">
                      Wheel Set
                    </h2>
                  </a>
                </li>
              </ul>
            </amp-carousel>
          </div>
        </section>
      </main>
    </App>
  </>
);
