import React from 'react';
import App from '../../components/App';

export default () => (
  <App>
    <main
      id="content"
      role="main"
      className="main commerce-checkout flex flex-wrap items-start pb3"
    >
      <section className="pt2 pb3 md-px4 md-pt4 md-pb7 col-12 md-col-8 md-px2">
        <h1 className="h3 mb2 px2">Checkout</h1>
        <ul className="list-reset flex flex-wrap justify-between h7 commerce-checkout-steps px2 pt2 pb2 md-ml2 md-px0">
          <li>
            <h2 className="h7">Shipping</h2>
          </li>
          <li>Billing</li>
          <li>Payment</li>
          <li>Confirmation</li>
        </ul>
        <hr className="xs-hide sm-hide mt2 mb1 ml2" />
        <div className="px2 mt4 md-mt3">
          <h3 className="h5 mb2">Delivery</h3>
          <div className="flex flex-wrap mxn2">
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="first-name"
                  id="first-name"
                  className="block border-none p0 m0"
                  placeholder="First name"
                />
                <label
                  htmlFor="first-name"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  First name
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="last-name"
                  id="last-name"
                  className="block border-none p0 m0"
                  placeholder="Last name"
                />
                <label
                  htmlFor="last-name"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  Last name
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="address-1"
                  id="address-1"
                  className="block border-none p0 m0"
                  placeholder="Address line 1"
                />
                <label
                  htmlFor="address-1"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  Address line 1
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="address-2"
                  id="address-2"
                  className="block border-none p0 m0"
                  placeholder="Address line 2"
                />
                <label
                  htmlFor="address-2"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  Address line 2
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="city"
                  id="city"
                  className="block border-none p0 m0"
                  placeholder="City/Region"
                />
                <label
                  htmlFor="city"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  City/Region
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="text"
                  value=""
                  name="postcode"
                  id="postcode"
                  className="block border-none p0 m0"
                  placeholder="Postcode"
                />
                <label
                  htmlFor="postcode"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  Postcode
                </label>
              </div>
            </div>
            <div className="col-12 md-col-6 px2">
              <div className="ampstart-input inline-block relative m0 p0 mb3 ">
                <input
                  type="email"
                  value=""
                  name="email"
                  id="email"
                  className="block border-none p0 m0"
                  placeholder="E-Mail Address"
                />
                <label
                  htmlFor="email"
                  className="absolute top-0 right-0 bottom-0 left-0"
                  aria-hidden="true"
                >
                  E-Mail Address
                </label>
              </div>
            </div>

            <div className="px2 pb3 flex flex-wrap col-12 md-pt3 md-pb3">
              <h4 className="h5 mt2 mb2 col-12">Select Delivery Method</h4>
              <div className="col-12">
                <div
                  className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                  on="change:AMP.setState({cart: {total: 723, shipping: 0}})"
                >
                  <input
                    type="radio"
                    value=""
                    name="delivery-method"
                    id="standard"
                    className="relative"
                    checked={false}
                  />
                  <label htmlFor="standard" className="" aria-hidden="true">
                    Standard (arrives in 2-3 days) - FREE
                  </label>
                </div>

                <div
                  className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                  on="change:AMP.setState({cart: {total: 753, shipping: 30}})"
                >
                  <input
                    type="radio"
                    value=""
                    name="delivery-method"
                    id="express"
                    className="relative"
                  />
                  <label htmlFor="express" className="" aria-hidden="true">
                    Express (arrives next day) - £30
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className="xs-hide sm-hide mt2 ml2" />
        <div className="mt2 px2">
          <h1 className="h5 xs-hide sm-hide">Your Basket</h1>
          <div className="commerce-cart-item flex flex-wrap items-center mx2 mb2 md-mx4 md-mt3 md-pb3">
            <div className="col-3 sm-col-2 md-col-2 lg-col-2">
              <amp-img
                className="commerce-cart-item-image"
                src="/img/e-commerce/product/product-8.jpg"
                width="1"
                height="1"
                layout="responsive"
                alt="Caliper Brakes"
                noloading=""
              >
                <div placeholder="" className="commerce-loader" />
              </amp-img>
            </div>
            <div className="commerce-cart-item-desc px1 col-6 sm-col-7 md-col-7 lg-col-7">
              <div className="h6 mb1">Caliper Brakes</div>
              <div>Fits most wheel sizes and designed to last long.</div>
            </div>
            <div className="commerce-cart-item-price col-3 h6 flex flex-wrap justify-around items-start">
              <span>£349</span>
              <span>1</span>
              <div
                role="button"
                className="inline-block commerce-cart-icon"
                tabIndex={0}
              >
                ✕
              </div>
            </div>
          </div>
          <div className="commerce-cart-item flex flex-wrap items-center mx2 mb2 md-mx4 md-mt3 md-pb3">
            <div className="col-3 sm-col-2 md-col-2 lg-col-2">
              <amp-img
                className="commerce-cart-item-image"
                src="/img/e-commerce/product/product-1.jpg"
                width="1"
                height="1"
                layout="responsive"
                alt="Sprocket Set"
                noloading=""
              >
                <div placeholder="" className="commerce-loader" />{' '}
              </amp-img>
            </div>
            <div className="commerce-cart-item-desc px1 col-6 sm-col-7 md-col-7 lg-col-7">
              <div className="h6 mb1">Sprocket Set</div>
              <div>Steel, designed htmlFor long lasting stability.</div>
            </div>
            <div className="commerce-cart-item-price col-3 h6 flex flex-wrap justify-around items-start">
              <span>£470</span>
              <span>1</span>
              <div
                role="button"
                className="inline-block commerce-cart-icon"
                tabIndex={0}
              >
                ✕
              </div>
            </div>
          </div>
          <div className="commerce-checkout-actions center">
            <a
              href="checkout_success"
              className="ampstart-btn ampstart-btn-secondary caps mt3"
            >
              Continue
            </a>
          </div>
        </div>
      </section>
      <section className="px2 pt3 pb4 md-px3 md-pt4 col-12 md-col-4 commerce-cart-summary commerce-side-panel">
        <h2 className="h5 md-mt3">Order Summary</h2>
        <dl className="flex flex-wrap">
          <dt className="col-10">Subtotal</dt>
          <dd className="m0 col-2 pb1 right-align">&#163;698</dd>
          <dt className="col-10 pb1">Shipping</dt>
          <dd
            className="m0 col-2 pb1 right-align"
          // data-amp-bind-text="'£' + cart.shipping"
          >
            &#163;0
          </dd>
          <dt className="commerce-cart-total col-10 pt2 pb2 bold mt3 mb2">
            Total
          </dt>
          <dd
            className="commerce-cart-total m0 col-2 pt2 pb2 right-align bold mt3 mb2"
          // data-amp-bind-text="'£' + cart.total"
          >
            &#163;723
          </dd>
        </dl>
        <p className="pb3">
          Donec id elit non mi porta gravida at eget metus. Donec ullamcorper
          nulla non metus auctor fringilla.
        </p>
      </section>
    </main>
  </App>
);
