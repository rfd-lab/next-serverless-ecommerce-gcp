import App from '../../components/App';
import Head from 'next/head';
import { AmpCustom } from 'components/e-commerce/SignupForm';
import React from 'react';
import SigninForm from 'components/e-commerce/SigninForm';

export default function SignIn() {
  return (
    <>
      <Head>
        <script
          async
          custom-element="amp-form"
          src="https://cdn.ampproject.org/v0/amp-form-0.1.js"
        ></script>
        {AmpCustom}
      </Head>
      <App>
        <main id="content" role="main" className="main pb2">
          <section className="md-col-6 px2 pt2 pb5 md-px4 md-pt4 md-pb7">
            <SigninForm />
          </section>
        </main>
      </App>
    </>
  );
}
