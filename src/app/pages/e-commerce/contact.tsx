import App from "../../components/App";

export default () => (
  <App>
    <main id="content" role="main" className="main pb2">
      <section className="md-col-6 px2 pt2 pb5 md-px4 md-pt4 md-pb7">
        <div className="flex flex-column">
          <h1 className="h3 mb2">Contact Us</h1>
          <p className="mb2">
            We’re here to keep you on two wheels. Give us a call with any
            questions, comments or feedback regarding any of our products. We’re
            available 9am-6pm on weekdays, 9-am-5pm at weekends.
          </p>
          <hr className="xs-hide sm-hide mt3 mb3" />

          <div className="ampstart-input inline-block relative m0 p0 mb3">
            <input
              type="text"
              value=""
              name="name"
              id="name"
              className="block border-none p0 m0"
              placeholder="Full name"
            />
            <label
              htmlFor="name"
              className="absolute top-0 right-0 bottom-0 left-0"
              aria-hidden="true"
            >
              Full name
            </label>
          </div>

          <div className="ampstart-input inline-block relative m0 p0 mb3">
            <input
              type="text"
              value=""
              name="email"
              id="email"
              className="block border-none p0 m0"
              placeholder="E-Mail address"
            />
            <label
              htmlFor="email"
              className="absolute top-0 right-0 bottom-0 left-0"
              aria-hidden="true"
            >
              E-Mail address
            </label>
          </div>

          <div className="ampstart-input inline-block relative m0 p0 mb3 mt3 mb5">
            <textarea
              name="mesage"
              id="message"
              className="block border-none  p0 m0"
              rows={2}
            ></textarea>
            <label
              htmlFor="message"
              className="absolute top-0 right-0 bottom-0 left-0"
              aria-hidden="true"
            >
              Type your message below
            </label>
          </div>
          <div>
            <button className="ampstart-btn ampstart-btn-secondary caps">
              send
            </button>
          </div>
        </div>
      </section>
    </main>
  </App>
);
