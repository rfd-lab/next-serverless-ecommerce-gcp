import App from '../../components/App';

export default () => (
  <App>
    <main id="content" role="main" className="main pb4">
      <section className="md-col-6 px2 pt2 pb3 md-px4 md-pt4 md-pb7">
        <div className="flex flex-column">
          <h1 className="h3 mb2">Success</h1>
          <p className="mb2">
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
            Duis mollis, est non commodo luctus, nisi erat porttitor ligula,
            eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed
            consectetur.
          </p>
          <div className="h2 mb3">832874JDB</div>
          <div>
            <a
              href="product_listing"
              className="ampstart-btn ampstart-btn-secondary caps"
            >
              continue shopping
            </a>
          </div>
        </div>
      </section>
    </main>
  </App>
);
