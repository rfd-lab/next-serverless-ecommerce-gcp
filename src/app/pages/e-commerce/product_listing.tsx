import fs from 'fs';
import path from 'path';
import { GetStaticProps } from 'next';
import Link from 'next/link';
import { dataScript } from '../../utils/amp/helpers';
import App from '../../components/App';
import { Product } from '../../constants/types';

interface ProductListingProps {
  products: Product[] | undefined;
}

function renderProducts(props: ProductListingProps): JSX.Element[] | undefined {
  return props.products?.map((product: Product) => (
    <Link href="product_details" key={product.name}>
      <a
        href="product_details"
        target="_self"
        className="commerce-listing-product text-decoration-none inline-block col-6 md-col-4 lg-col-3 px1 mb2 md-mb4 relative"
      >
        <div className="flex flex-column justify-between">
          <div>
            <amp-img
              className="commerce-listing-product-image mb2"
              src={product.image}
              width="340"
              height="340"
              layout="responsive"
              alt={product.name}
              noloading=""
            >
              <div placeholder="" className="commerce-loader" />
            </amp-img>
            <h2 className="commerce-listing-product-name h6">{product.name}</h2>
            {product.description}
          </div>
          <div className="h6 mt1">
            &#163;
            {product.price}
          </div>
        </div>
      </a>
    </Link>
  ));
}

function ProductListing(props: ProductListingProps): JSX.Element {
  const data = { category: 'all', filter: 'high-low' };

  return (
    <App>
      <main id="content" role="main" className="main commerce-listing">
        <amp-state id="products">{dataScript(data)}</amp-state>

        <amp-img
          className="commerce-listing-banner xs-hide sm-hide"
          src="/img/e-commerce/wide-listings-hero.jpg"
          width="2560"
          height="400"
          layout="responsive"
          alt="Product listing"
          noloading=""
        >
          <div placeholder="" className="commerce-loader" />
        </amp-img>

        <section className="commerce-listing-content mx-auto flex flex-wrap pb4">
          <div className="col-3 xs-hide sm-hide flex flex-column">
            <div className="commerce-side-panel pt4 pr4 self-center">
              <h2 className="h5 mb2">Categories</h2>
              <div
                className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                on="change: AMP.setState({products: {category: &#39;all&#39;}})"
              >
                <input
                  type="radio"
                  value=""
                  name="category"
                  id="all"
                  className="relative"
                  defaultChecked={false}
                />
                <label htmlFor="all" className="" aria-hidden="true">
                  All
                </label>
              </div>

              <div
                className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                on="change: AMP.setState({products: {category: &#39;bikes&#39;}})"
              >
                <input
                  type="radio"
                  value=""
                  name="category"
                  id="bikes"
                  className="relative"
                />
                <label htmlFor="bikes" className="" aria-hidden="true">
                  Bikes
                </label>
              </div>

              <div
                className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                on="change: AMP.setState({products: {category: &#39;accessories&#39;}})"
              >
                <input
                  type="radio"
                  value=""
                  name="category"
                  id="accessories"
                  className="relative"
                />
                <label htmlFor="accessories" className="" aria-hidden="true">
                  Accessories
                </label>
              </div>

              <div
                className="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 "
                on="change: AMP.setState({products: {category: &#39;components&#39;}})"
              >
                <input
                  type="radio"
                  value=""
                  name="category"
                  id="components"
                  className="relative"
                />
                <label htmlFor="components" className="" aria-hidden="true">
                  Components
                </label>
              </div>
            </div>
          </div>

          <div className="col-12 md-col-7 pt2 pb3 md-px4 md-pt1 md-pb7">
            <div className="md-commerce-header relative md-flex flex-wrap items-center md-mx0 md-mb2">
              <h1 className="h3 mb2 md-mt2 md-mb2 md-ml0 flex-auto px2">
                Products
              </h1>
              <div className="commerce-listing-filters pt2 pb2 mb3 md-mb0">
                <div className="commerce-select-wrapper inline-block md-mr1 pl2 md-hide lg-hide">
                  <label htmlFor="categories" className="bold caps h6 md-h7">
                    Show:
                  </label>
                  <select
                    name="categories"
                    id="categories"
                    className="commerce-select h6 md-h7"
                    on="change: AMP.setState({products: {category: event.value}})"
                  >
                    <option value="all">all</option>
                    <option value="bikes">bikes</option>
                    <option value="accessories">accessories</option>
                    <option value="components">components</option>
                  </select>
                </div>
                <div className="commerce-select-wrapper inline-block  ">
                  <label htmlFor="price" className="bold caps h6 md-h7">
                    Sort by:
                  </label>
                  <select
                    name="price"
                    id="price"
                    className="commerce-select h6 md-h7"
                    on="change: AMP.setState({products: {filter: event.value}})"
                  >
                    <option value="high-low"> Price: High-Low</option>
                    <option value="low-high"> Price: Low-High</option>
                  </select>
                </div>
              </div>
            </div>
            {renderProducts(props)}
          </div>
        </section>
      </main>
    </App>
  );
}

export default ProductListing;

export const getStaticProps: GetStaticProps = async () => {
  const filePath = path.join(process.cwd(), 'data', 'products.json');
  const fileContent = fs.readFileSync(filePath, 'utf8');
  const data = JSON.parse(fileContent);

  return {
    props: {
      products: data.items
    }
  };
};
