import React from 'react';
import App from '../../components/App';

export default (): JSX.Element => (
  <App>
    <main id="content" role="main" className="main commerce-landing">
      <section className="commerce-hero-content-wrapper center overflow-hidden">
        <a href="product_listing" className="text-decoration-none block">
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/narrow-landing-01.jpg"
            width="750"
            height="573"
            layout="responsive"
            media="(max-width: 52.06rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/wide-landing-01.jpg"
            width="2560"
            height="800"
            layout="responsive"
            media="(min-width: 52.07rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <div className="commerce-hero-content commerce-hero-content-theme-primary flex justify-center flex-column md-mx-auto md-p0">
            <div className="commerce-hero-align md-mx-auto">
              <h2 className="commerce-hero-content-title h4 md-h1 caps md-mb2 md-pl4">
                The Essentials
              </h2>
              <p className="commerce-hero-content-body">
                These are the key frames, clothing and accessories every rider
                needs.
              </p>
              <div className="ampstart-btn ampstart-btn-secondary caps md-ml4">
                shop
              </div>
            </div>
          </div>
        </a>
      </section>
      <section className="commerce-hero-content-wrapper center overflow-hidden">
        <a href="blog_listing" className="text-decoration-none block">
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/narrow-landing-02.jpg"
            width="750"
            height="573"
            layout="responsive"
            media="(max-width: 52.06rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/wide-landing-02.jpg"
            width="2560"
            height="800"
            layout="responsive"
            media="(min-width: 52.07rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <div className="commerce-hero-content commerce-hero-content-theme-primary flex justify-center flex-column md-mx-auto md-p0">
            <div className="commerce-hero-align md-mx-auto">
              <h2 className="commerce-hero-content-title h4 md-h1 caps md-mb2 md-pl4">
                Lost Highways
              </h2>
              <p className="commerce-hero-content-body">
                Go the distance and discover new levels of endurance along the
                way. Explore the world’s 10 toughest randonnées.
              </p>
              <div className="ampstart-btn ampstart-btn-secondary caps md-ml4">
                read
              </div>
            </div>
          </div>
        </a>
      </section>
      <section className="commerce-hero-content-wrapper center overflow-hidden">
        <a href="product_listing" className="text-decoration-none block">
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/narrow-landing-03.jpg"
            width="750"
            height="573"
            layout="responsive"
            media="(max-width: 52.06rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/wide-landing-03.jpg"
            width="2560"
            height="800"
            layout="responsive"
            media="(min-width: 52.07rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <div className="commerce-hero-content commerce-hero-content-theme-secondary flex justify-center flex-column md-mx-auto md-p0">
            <div className="commerce-hero-align md-mx-auto">
              <h2 className="commerce-hero-content-title h4 md-h1 caps md-mb2 md-pl4">
                Sprocket Set 3 Series
              </h2>
              <p className="commerce-hero-content-body">
                The steel-framed complete bike that combines peerless
                performance with timeless style.
              </p>
              <div className="ampstart-btn ampstart-btn-secondary caps md-ml4">
                shop
              </div>
            </div>
          </div>
        </a>
      </section>
      <section className="commerce-hero-content-wrapper center overflow-hidden">
        <a href="blog_listing" className="text-decoration-none block">
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/narrow-landing-04.jpg"
            width="750"
            height="573"
            layout="responsive"
            media="(max-width: 52.06rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <amp-img
            className="commerce-hero-image"
            src="/img/e-commerce/wide-landing-04.jpg"
            width="2560"
            height="800"
            layout="responsive"
            media="(min-width: 52.07rem)"
            alt="Hero Landing 1"
            noloading=""
          >
            <div placeholder="" className="commerce-loader" />
          </amp-img>
          <div className="commerce-hero-content commerce-hero-content-theme-primary flex justify-center flex-column md-mx-auto md-p0">
            <div className="commerce-hero-align md-mx-auto">
              <h2 className="commerce-hero-content-title h4 md-h1 caps md-mb2 md-pl4">
                No Sleep &#39;til Brooklyn
              </h2>
              <p className="commerce-hero-content-body">
                Early mornings, black coffee, carbs. We spend a week on the road
                with New York&#39;s Five Borough Cycle Club.
              </p>
              <div className="ampstart-btn ampstart-btn-secondary caps md-ml4">
                read
              </div>
            </div>
          </div>
        </a>
      </section>
    </main>
  </App>
);
