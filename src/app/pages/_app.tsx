import React from 'react';
import { AppProps } from 'next/app';
import { GetServerSideProps } from 'next';
import { ThemeProvider } from 'styled-components';
import Head from 'next/head';

const theme = {
  colors: {
    primary: '#000'
  }
};

function EnhancedApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <React.Fragment>
      <Head>
        <title>AMP e-commerce template</title>
      </Head>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </React.Fragment>
  );
}

export default EnhancedApp;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const getServerSideProps: GetServerSideProps = async (_context) => {
  return {
    props: {}
  };
};
