import { NextApiRequest, NextApiResponse } from 'next';
import { createUserAccount } from 'utils/auth/authService';
import { SignUpResponse } from 'constants/types';

export const config = {
  api: {
    bodyParser: false
  }
};

export default async (
  req: NextApiRequest,
  res: NextApiResponse<SignUpResponse>
) => {
  if (req.method === 'POST') {
    if (!req.body) {
      return res.status(400);
    }

    const { statusCode, error, user } = await createUserAccount(req.body);

    console.debug(
      `method: ${req.method} - url: /api/auth/signup  {
        statusCode: ${statusCode},
        error: ${error},
        user: ${JSON.stringify(user)}
      }`
    );

    res.statusCode = statusCode || 200;

    if (error) {
      res.end(JSON.stringify(error));
    }

    if (user) {
      res.setHeader(
        'AMP-Redirect-To',
        'https://sunshine-ba626.firebaseapp.com/'
      );

      res.setHeader('Access-Control-Expose-Headers', 'AMP-Redirect-To');

      res.end(JSON.stringify(user));
    }
  }
};
