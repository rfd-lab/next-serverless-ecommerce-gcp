import { NextApiRequest } from 'next';
import { loginUser } from 'utils/auth/authService';
import { SignInResponse } from 'constants/types';
import admin from '../../../utils/auth/firebaseAdminService';
import cookies from '../../../utils/middlewares/cookies';

export const config = {
  api: {
    bodyParser: false
  }
};

const code = 'api/signin';

export default cookies(async (req: NextApiRequest, res: SignInResponse) => {
  if (!req.body) {
    return res.status(400);
  }

  const csrfToken = req.body.csrfToken.toString();

  if (csrfToken !== req.cookies['csrfToken']) {
    const error = {
      statusCode: 401,
      error: { code, message: 'Unauthorized request' }
    };

    res.status(error.statusCode);

    res.end(JSON.stringify(error));
  }

  if (req.method === 'POST') {
    const { email, password } = req.body;
    const { statusCode, error, credential } = await loginUser(email, password);
    const expiresIn = 60 * 60 * 24 * 5 * 1000;

    res.statusCode = statusCode || 200;

    if (error) {
      res.end(JSON.stringify(error));
    }

    if (credential && credential.user) {
      const idToken = await credential.user.getIdToken();

      const sessionCookie = admin
        .auth()
        .createSessionCookie(idToken, { expiresIn });

      const options = { maxAge: expiresIn, httpOnly: true, secure: true };

      res.cookie('session', sessionCookie, options);

      res.end(JSON.stringify({ status: 'success' }));
    }
  }
});
