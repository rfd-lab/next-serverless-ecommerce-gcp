/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { createStore, applyMiddleware, Store as ReduxStore } from 'redux';
import { createLogger } from 'redux-logger';
// import { composeWithDevTools } from 'redux-devtools-extension';
import { composeWithDevTools } from 'remote-redux-devtools';

const initialState = {
  lastUpdate: 0,
  light: false,
  count: 0
};

export type Store = ReduxStore<typeof initialState>;

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TICK':
      return {
        ...state,
        lastUpdate: action.lastUpdate,
        light: !!action.light
      };
    case 'INCREMENT':
      return {
        ...state,
        count: state.count + 1
      };
    case 'DECREMENT':
      return {
        ...state,
        count: state.count - 1
      };
    case 'RESET':
      return {
        ...state,
        count: initialState.count
      };
    default:
      return state;
  }
};
export const initializeStore = (preloadedState = initialState) => {
  const dev: boolean = process.env.NODE_ENV !== 'production';

  const middlewares = dev ? [createLogger()] : [];

  const composeEnhancers = composeWithDevTools({
    realtime: true,
    port: 8000,
    hostname: 'localhost'
  });

  return createStore(
    reducer,
    preloadedState,
    // composeWithDevTools(applyMiddleware())
    dev
      ? composeEnhancers(applyMiddleware(...middlewares))
      : applyMiddleware(...middlewares)
  );
};
