import React from 'react';
import renderer from 'react-test-renderer';
import preloadAll from 'jest-next-dynamic';
import EcommerceIndexPage from '../pages/e-commerce/index';

beforeAll(async () => {
  await preloadAll();
});

it('renders homepage unchanged', () => {
  const tree = renderer.create(<EcommerceIndexPage />).toJSON();
  expect(tree).toMatchSnapshot();
});
