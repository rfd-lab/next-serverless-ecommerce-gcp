// The elements you list here will be accepted, and only with the attributes that you include here
declare namespace JSX {
  interface IntrinsicElements {
    [elemName: string]: any;
  }

  interface AmpImg {
    alt?: string;
    src?: string;
    width?: string;
    height?: string;
    layout?: string;
    children?: any;
    className?: string;
    noloading?: string;
    srcset?: string;
    media?: string;
    option?: string;
    selected?: string;
    role?: string;
    tabindex?: string;
  }

  interface AmpList {
    src?: string;
    height?: string;
    width?: string;
    layout?: string;
    className?: string;
    children?: any;
    dangerouslySetInnerHTML?: { __html: string };
  }

  interface AMPDivElement extends Partial<HTMLDivElement> {
    name?: string;
    id?: string;
    role?: string;
    'aria-label'?: string;
    on?: string;
    tabindex?: string;
    children?: [HTMLElement] | unknown;
    className?: string;
    placeholder?: string;
    option?: string;
    selected?: string;
  }

  interface AMPButtonElement extends Partial<HTMLButtonElement> {
    on?: string;
    children?: [HTMLElement] | unknown;
    dangerouslySetInnerHTML?: { __html: string };
    type?: string;
  }

  interface AMPTemplateElement extends Partial<HTMLTemplateElement> {
    type?: string;
    dangerouslySetInnerHTML?: { __html: string };
  }

  interface IntrinsicElements {
    'amp-/img': AmpImg;
    div: AMPDivElement;
    template: AMPTemplateElement;
    'amp-list': AmpList;
    select: AMPDivElement;
    button: AMPButtonElement;
  }
}
