import express from 'express';
import { handle } from '../handlers/appHandler';

const router = express.Router();

router.get('/*', (req, res) => {
  handle(req, res);
});

export default router;
