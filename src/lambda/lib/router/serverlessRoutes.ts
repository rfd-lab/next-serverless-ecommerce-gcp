/* eslint-disable no-console */
import * as functions from 'firebase-functions';
import path from 'path';
import { pagesDir, publicDir } from '../constants/index';

const assets = functions.https.onRequest(async (req, res) => {
  const resourcePath = path.join(publicDir, req.path);
  res.sendFile(resourcePath);
});

const index = functions.https.onRequest(async (_req, res) => {
  const pagePath = path.join(pagesDir, 'index.html');
  res.sendFile(pagePath);
});

const pages = functions.https.onRequest(async (req, res) => {
  const pagePath = path.join(pagesDir, 'e-commerce', path.basename(req.path));
  res.sendFile(pagePath);
});

export default {
  index,
  assets,
  pages,
};
