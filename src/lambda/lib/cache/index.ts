import LRUCache from 'lru-cache';

const dev = process.env.NODE_ENV !== 'production';

const ssrCache = new LRUCache({
  max: 100,
  maxAge: 1000 * 60 * 60,
});

const renderAndCache = async (
  req: any,
  res: any,
) => {
  const key = req.url;

  if (ssrCache.has(key)) {
    res.setHeader('x-cache', 'HIT');
    res.send(ssrCache.get(key));
    return;
  }

  try {
    const html = await res.readFileSync(req.path);

    if (dev || res.statusCode !== 200) {
      res.setHeader('x-cache', 'SKIP');
      res.send(html);
      return;
    }

    ssrCache.set(key, html);
    res.setHeader('x-cache', 'MISS');
    res.send(html);
  } catch (err) {
    await res.error(err);
  }
};

export default renderAndCache;
