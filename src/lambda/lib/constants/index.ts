import path from 'path';

export const rootDir = process.cwd();
export const pagesDir = path.join(rootDir, 'app', 'serverless', 'pages');
export const publicDir = path.join(rootDir, 'public');
