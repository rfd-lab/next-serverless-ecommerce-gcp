import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import logger from 'morgan';
import { app, handle } from '../handlers/appHandler';

const server = express();

app.prepare().then(() => {
  server.use(cors());
  server.use(logger('dev'));
  server.use(bodyParser.json());
  server.use(express.urlencoded({ extended: false }));
  server.use(cookieParser());
  server.all('*', (req, res) => handle(req, res));
});

export default server;
