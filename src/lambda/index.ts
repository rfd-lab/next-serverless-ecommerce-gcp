import server from './lib/server/index';

const functions = require('firebase-functions');

exports.app = functions.https.onRequest(server);
